/*
 * SCANCommand.cc
 *
 *  Created on: Apr 19, 2014
 *      Author: Seven
 *
 * List all network, or a specified server clients list
 * It has the ability to omit the weblist specified exceptioned masks.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 * $Id: SCANCommand.cc,v 1.0 2014/04/19 16:45:42 Seven Exp $
 */


#include        <string>
#include        <sstream>

#include        "StringTokenizer.h"
#include        "cservice.h"
#include        "levels.h"
#include        "responses.h"
#include        "dbHandle.h"
#include		"Network.h"

namespace gnuworld
{
using std::string ;
using std::endl ;
using std::ends ;
using std::stringstream ;

bool SCANCommand::Exec( iClient* theClient, const string& Message )
{

bot->incStat("COMMANDS.SCAN");

StringTokenizer st( Message ) ;
if( st.size() < 2 )
{
	Usage(theClient);
	return true;
}

bool showAll = false;
bool dubious = false;
string scanServer;
unsigned int minClones = 1;
string option;
std::list<string> clientList;
iServer* theServer;

/*
 *  Fetch the sqlUser record attached to this client. If there isn't one,
 *  they aren't logged in - tell them they should be.
 */
sqlUser* theUser = bot->isAuthed(theClient, true);
if (!theUser)
        {
        return false;
        }

/*
 *  Check the user has sufficient access for this command..
 */
int level = bot->getAdminAccessLevel(theUser);
if (level < level::scanhost)
{
        bot->Notice(theClient,
                bot->getResponse(theUser,
                        language::insuf_access,
                        string("You have insufficient access to perform that command")));
        return false;
}

if (st[1][0] == '-')
	option = string_lower(st[1].c_str());
else
{
	scanServer = st[1];
	if (st.size() > 2)
		option = string_lower(st[2].c_str());
	else
		option = "-all";
}

if ((string_upper(scanServer) == "NICK") || (string_upper(scanServer) == "NICKNAME"))
{
	scanServer.clear();
	option = string_upper(st[1].c_str());
}

elog << "option = " << option << std::endl;
elog << "scanServer = " << scanServer << std::endl;

if (!scanServer.empty())
{
	theServer = Network->findServerName(scanServer);
	if (NULL == theServer)
	{
		bot->Notice(theClient, "ERROR: Unable to find server: %s", scanServer.c_str());
		return false ;
	}
	scanServer = theServer->getName();
}

if (option == "-minclones")
{
	if (((scanServer.empty()) && (st.size() < 3)) || ((!scanServer.empty()) && (st.size() < 4)))
	{
		Usage(theClient);
		return true;
	}
	string strCloneNr;
	if (scanServer.empty())
		strCloneNr = st[2];
	else
		strCloneNr = st[3];
	if (!IsNumeric(strCloneNr))
	{
		bot->Notice(theClient, "Invalid integer number");
		return false;
	}
	minClones = ::atoi(strCloneNr.c_str());
	if (scanServer.empty())
		bot->Notice(theClient, "Scanning after %i clones on all servers", minClones);
	else
		bot->Notice(theClient, "Scanning after %i clones on server %s", minClones, scanServer.c_str());
	return true;
}

if (option == "-nochans")
{
	dubious = true;
	if (scanServer.empty())
		bot->Notice(theClient, "Scanning after NoChans clients on all servers");
	else
		bot->Notice(theClient, "Scanning after NoChans clients on server %s", scanServer.c_str());
	return true;
}

if (option == "-dubious")
{
	dubious = true;
	if (scanServer.empty())
		bot->Notice(theClient, "Scanning after dubious clients on all servers");
	else
		bot->Notice(theClient, "Scanning after dubious clients on server %s", scanServer.c_str());
	return true;
}

if ((option == "-all") || (option == "-allservers"))
{
	showAll = true;
	xNetwork::const_clientIterator cItr = Network->clients_begin();
	if (scanServer.empty())
	{
		bot->Notice(theClient, "Listing all clients clients on all servers:");
		for ( ; cItr != Network->clients_end(); ++cItr)
		{
			iClient* tmpClient = cItr->second;
			//clientList.push_back(tmpClient->getRealNickUserHost());
			bot->Notice(theClient, "   %s", tmpClient->getRealNickUserHost().c_str());
		}
		//if (clientList.size() > 0)
		//for (std::list<string>::iterator itr = clientList.begin(); itr != clientList.end(); itr++)
		//{
		//	bot->Notice(theClient, *itr);
		//}
	}
	else
	{
		bot->Notice(theClient, "Listing all clients on server %s:", scanServer.c_str());
		//if (cItr != Network->clients_end())
		for ( ; cItr != Network->clients_end(); ++cItr)
		{
			iClient* tmpClient = cItr->second;
			if (tmpClient->getIntYY() == theServer->getIntYY())
			{
				//clientList.push_back(tmpClient->getRealNickUserHost());
				bot->Notice(theClient, "   %s", tmpClient->getRealNickUserHost().c_str());
			}
		}
		//if (clientList.size() > 0)
		//for (std::list<string>::iterator itr = clientList.begin(); itr != clientList.end(); itr++)
		//{
		//	bot->Notice(theClient, *itr);
		//}
	}
	return true;
}

if ((option == "NICK") || (option == "NICKNAME"))
{
	string nickOwner = bot->NickIsRegisteredTo(st[2]);
	if (!nickOwner.empty())
		bot->Notice(theClient, "Nickname is registered to %s", nickOwner.c_str());
	else
		bot->Notice(theClient, bot->getResponse(theUser, language::no_match).c_str());
}

return true;
}
} //namespace gnuworld




