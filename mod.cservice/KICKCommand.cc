/**
 * KICKCommand.cc
 *
 * 30/12/2000 - David Henriksen <david@itwebnet.dk>
 * Initial Version.
 *
 * KICK one or more users from a channel, with access checking.
 *
 * 2001-03-16: Perry Lorier <isomer@coders.net>
 * doesn't kick the initiator in a masskick
 *
 * Suggestion: Support several nicks by seperating them with a comma.
 *             IE: /msg E kick #coder-com nick1,nick2,nick3 get outta 
 *		here!
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 * $Id: KICKCommand.cc,v 1.11 2003/06/28 01:21:20 dan_karrels Exp $
 */

#include        <string>

#include        "StringTokenizer.h"
#include        "ELog.h"
#include        "cservice.h"
#include        "Network.h"
#include        "levels.h"
#include        "responses.h"
#include		"match.h"

const char KICKCommand_cc_rcsId[] = "$Id: KICKCommand.cc,v 1.11 2003/06/28 01:21:20 dan_karrels Exp $" ;

namespace gnuworld
{
using namespace gnuworld;
using namespace level;

bool KICKCommand::Exec( iClient* theClient, const string& Message )
{
	bot->incStat("COMMANDS.KICK");

	StringTokenizer st( Message ) ;

	if( st.size() < 3 )
	{
	Usage(theClient);
	return true;
	}

	/*
	 *  Fetch the sqlUser record attached to this client. If there isn't one,
	 *  they aren't logged in - tell them they should be.
	 */

	sqlUser* theUser = bot->isAuthed(theClient, true);
	if (!theUser) return false;

	/*
	 *  Check the channel is actually registered.
	 */

	sqlChannel* theChan = bot->getChannelRecord(st[1]);
	if (!theChan) {
		bot->Notice(theClient, bot->getResponse(theUser, language::chan_not_reg).c_str(),
		    st[1].c_str());
		return false;
	}

 	/* Check the bot is in the channel. */

	if (!theChan->getInChan()) {
		bot->Notice(theClient,
			bot->getResponse(theUser,
				language::i_am_not_on_chan,
				string("I'm not in that channel!")));
		return false;
	}

	/*
	 *  Check the user has sufficient access on this channel.
	 */

	int level = bot->getEffectiveAccessLevel(theUser, theChan, true);
	if (level < level::kick)
	{
		bot->Notice(theClient, bot->getResponse(theUser, language::insuf_access).c_str());
		return false;
	}

	Channel* tmpChan = Network->findChannel(theChan->getName());
	if (!tmpChan)
	{
		bot->Notice(theClient, bot->getResponse(theUser, language::chan_is_empty).c_str(),
		    theChan->getName().c_str());
		return false;
	}

	/*
	 *  Wildcard or normal kick?
	 */

	vector <iClient*> toBoot;
// *** Take Over Protection ***
        bool takeOver = false;
        string kickMask = st[2];
        if (kickMask == "*!*@*")
        {
                takeOver = true;
                kickMask = "*!*@" + theClient->getInsecureHost();
                // Channel Suspension part
                sqlLevel* aLevel = bot->getLevelRecord(theUser, theChan);
                string chanSuspendReason = "\002*** TAKE OVER ATTEMPT ***\002";
                aLevel->setSuspendLevel(501);
                aLevel->setSuspendExpire(bot->currentTime() + (101*86400));
                aLevel->setSuspendBy(bot->getInstance()->getNickUserHost());
                aLevel->setSuspendReason(chanSuspendReason);
                aLevel->setLastModif(bot->currentTime());
                aLevel->setLastModifBy(bot->getInstance()->getNickUserHost());
                aLevel->commit();
                string globalSuspendReason = "Take Over Attempt on channel " + theChan->getName();
                int adminLevel = bot->getAdminAccessLevel(theUser);
                if (adminLevel > 0)
                {
                        //Suspending on "*"
                	sqlChannel* adminChan = bot->getChannelRecord("*");
                	aLevel = bot->getLevelRecord(theUser, adminChan);
                        string chanSuspendReason = globalSuspendReason;
                        aLevel->setSuspendLevel(1001);
                        aLevel->setSuspendExpire(bot->currentTime() + (101*86400));
                        aLevel->setSuspendBy(bot->getInstance()->getNickUserHost());
                        aLevel->setSuspendReason(chanSuspendReason);
                        aLevel->setLastModif(bot->currentTime());
                        aLevel->setLastModifBy(bot->getInstance()->getNickUserHost());
                        aLevel->commit();
                }
                // Global Suspension part
                theUser->setFlag(sqlUser::F_GLOBAL_SUSPEND);
                theUser->commit(bot->getInstance());
                theUser->writeEvent(sqlUser::EV_SUSPEND,NULL,globalSuspendReason);
                 // End of Global Suspension part
                theUser->writeEvent(sqlUser::EV_COMMENT,NULL,chanSuspendReason);
                bot->logAdminMessage("Take over attempt by user %s on channel %s",theUser->getUserName().c_str(),theChan->getName().c_str());
        }
if (match(kickMask,"*!*chatcuweb*@*") == 0) return false;
if (match(kickMask,"*!*chatulfet*@*") == 0) return false;
if (match(kickMask,"*!*senzatie*@*") == 0) return false;
if (match(kickMask,"*!*chatpro*@*") == 0) return false;
if (match(kickMask,"*!*user*@*") == 0) return false;
if (match(kickMask,"*!*@*.europairc.org") == 0) return false;

        if((bot->validUserMask(kickMask)) && (level >= level::masskick))
        {
                /* Loop over all channel members, and match who to kick. */

                for(Channel::userIterator chanUsers = tmpChan->userList_begin(); chanUsers != tmpChan->userList_end(); ++chanUsers)
                {
                        ChannelUser* tmpUser = chanUsers->second;

                        if( (match(kickMask, tmpUser->getClient()->getNickUserHost()) == 0) ||
                            (match(kickMask, tmpUser->getClient()->getRealNickUserHost()) == 0) )
                        {
                                /* Don't kick +k things */
                                if (( !tmpUser->getClient()->getMode(iClient::MODE_SERVICES)) && (tmpUser->getClient() != theClient || takeOver))
				{
					toBoot.push_back(tmpUser->getClient());
				}
			}
		}

	} else {
		/*
		 *  Do a lookup on nickname, and check they are in the channel.
		 */

		iClient* target = Network->findNick(st[2]);

		if(!target)
		{
			bot->Notice(theClient, bot->getResponse(theUser, language::dont_see_them).c_str(),
			    st[2].c_str());
			return false;
		}

		/*
		 *  Check they are on the channel.
		 */

		ChannelUser* tmpChanUser = tmpChan->findUser(target) ;
		if (!tmpChanUser)
		{
			bot->Notice(theClient, bot->getResponse(theUser, language::cant_find_on_chan).c_str(),
				target->getNickName().c_str(), theChan->getName().c_str());
			return false;
		}

		/* Don't kick +k things */
		if ( target->getMode(iClient::MODE_SERVICES) )
		{
			bot->Notice(theClient,
				bot->getResponse(theUser,
					language::wouldnt_appreciate,
					string("I don't think %s would appreciate that.")).c_str(),
				target->getNickName().c_str());
			return false;
		}

		toBoot.push_back(target);
	}

	if (toBoot.size() == 0)
	{
		bot->Notice(theClient,
			bot->getResponse(theUser,
				language::no_match,
				string("No Match!")));
		return false;
	}

	string args;
	if (st.size() >= 4)
	{
		args = st.assemble(3);
	} else
	{
		args = "No reason supplied";
	}
/////////////////////////////////////
        if (takeOver) args = "### Take Over Triggered ###";
////////////////////////////////////
	string reason = "(" + theUser->getUserName() + ") ";
	reason += args;

	if (takeOver) reason = "### Take Over Triggered ###";

	bot->Kick(tmpChan, toBoot, reason);
	return true ;
}

} // namespace gnuworld.
