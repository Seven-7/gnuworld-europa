/* HELLOCommand.cc */

#include	<string>

#include	"cservice_config.h"
#include	"StringTokenizer.h"
#include	"ELog.h"
#include	"Network.h"
#include	"ip.h"
#include	"levels.h"
#include	"dbHandle.h"
#include	"cservice.h"

const char HELLOCommand_cc_rcsId[] = "$Id: HELLOCommand.cc,v 1.1 2005/04/03 22:11:42 dan_karrels Exp $" ;

namespace gnuworld
{
using std::string ;
using std::endl ;
using std::ends ;
using std::stringstream ;

bool HELLOCommand::Exec( iClient* theClient, const string& Message )
{
#ifdef ALLOW_HELLO

sqlUser* theUser = bot->isAuthed(theClient, false);
int admLevel = 0;
if (theUser)
{
	admLevel = bot->getAdminAccessLevel(theUser);
	if (admLevel == 0)
	{
	#ifndef ALLOW_USERS_HELLO
		bot->Notice(theClient, "HELLO command is disabled. Use webinterface instead (if available)");
		return true;
	#endif
		bot->Notice(theClient, "You can't create another "
			"account when you already have one!");
	        return false;
        }
}
else
{
#ifndef ALLOW_USERS_HELLO
	bot->Notice(theClient, "HELLO command is disabled. Use webinterface instead (if available)");
	return true;
#endif
}

bot->incStat("COMMANDS.HELLO");

StringTokenizer st( Message ) ;
if (( st.size() < 3 ) || (st.size() > 3))
        {
        Usage(theClient);
        return true;
        }

const char validChars[]
        = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

/*
 * Check this IP hasn't already tried in the last 48 hours.
 */
cservice::helloIPListType::iterator itr = bot->helloIPList.find( theClient->getIP() ) ;

if (admLevel == 0) //(&& !theUser)) //only normal users has the one IP/user restriction, any Admin don't
if (itr != bot->helloIPList.end())
{
        /*
         * Has it been 48hours already?
         */
        if (bot->currentTime() < itr->second)
        {
                bot->Notice(theClient, "Sorry, an account has "
                        "already been registered from this IP within "
                        "the last %i hours. "
                        "Please wait and try again, or contact cservice.",
                        bot->helloBlockPeriod / 3600);

                return false;
        }
}

sqlUser* newUser = bot->getUserRecord(st[1].c_str());
if(newUser)
        {
        bot->Notice(theClient, "This username already exists!");
        return false;
        }

/*
 * Check the username contains valid characters/correct
length.
 */
if (  (st[1].size() < 2) || (st[1].size() > 12)  )
        {
        bot->Notice(theClient, "You username must be 2 to 12 chars long.");
        return false;
        }

string theUserName = st[1];
bool badName = false;

for( string::const_iterator ptr = theUserName.begin() ;
        ptr != theUserName.end() ; ++ptr )
        {
        /*
         * 62 entries in the table. 26 + 26 + 10 digits.
         */

        bool found = false;
        for (int f = 0; f < 62; f++)
                {
                if(*ptr == validChars[f])
                        {
                        found = true;
                        }
                }
        if (!found)
                {
                badName = true;
                }
        }

if (badName)
        {
        bot->Notice(theClient, "Your useraname must be made of letters (A-Z, a-z) and numbers (0-9).");
        return false;
        }

/*
 * Check if user_name,email,verification answer is in NOREG/LOCKED
 */

if (admLevel < level::hello)
{
stringstream theQuery;
theQuery        << "SELECT user_name,type,reason FROM noreg WHERE user_name <> 0"
                << ends;
if (!bot->SQLDb->Exec(theQuery, true))
{       bot->logDebugMessage("Error on HELLO.NoregEmailQuery");
#ifdef LOG_SQL
        //elog << "sqlQuery> " << theQuery.str().c_str() << endl;
        elog << "Hello.NoregEmailQuery> SQL Error: "
             << bot->SQLDb->ErrorMessage()
             << endl ;
#endif
        return false;
} else if (bot->SQLDb->Tuples() != 0)
{
        unsigned short type;
        string user_name;
        string reason;
        for (unsigned int i = 0 ; i < bot->SQLDb->Tuples(); i++)
        {
                user_name = bot->SQLDb->GetValue(i,0);
                type = atoi(bot->SQLDb->GetValue(i,1));
                reason = bot->SQLDb->GetValue(i,2);
                if ((user_name.size() > 0) && (user_name != "*"))
                {
                        if ((type < 6) && (!match(user_name,st[1])))
                        {
                                if (type < 2) bot->Notice(theClient,"Invalid username (NOREG)");
                                if (type == 5) bot->Notice(theClient,"Invalid username (LOCKED)");
                                bot->Notice(theClient,"Usernames matching %s are disallowed for the following reason:",user_name.c_str());
                                bot->Notice(theClient,reason.c_str());
                                return false;
                        }
                }
        }
}
}
/*
bot->helloIPList.erase(theClient->getIP());
bot->helloIPList.insert(
        std::make_pair(theClient->getIP(),
                bot->currentTime() + bot->helloBlockPeriod) );
*/
//} //admLevel

//bot->Notice(theClient,"Username %s created successfully.",st[1].c_str());
//return true;

/*
 * We need to give this user a password
 */
string plainpass = st.assemble(2);

string cryptpass = bot->CryptPass(plainpass);
string updatedBy = "HELLO used by: ";
updatedBy += theClient->getNickUserHost().c_str();

newUser = new (std::nothrow) sqlUser(bot->SQLDb);
newUser->setUserName(escapeSQLChars(st[1].c_str()));
//newUser->setEmail(escapeSQLChars(st[2]));
//newUser->setVerifNr(verifID);
//newUser->setVerifData(verifAnswer);
newUser->setPassword(cryptpass.c_str());
newUser->setLastUpdatedBy(updatedBy);
newUser->setCreatedTS(bot->currentTime());
newUser->setInstantiatedTS(bot->currentTime());
newUser->setSignupIp(xIP(theClient->getIP()).GetNumericIP());
newUser->Insert();

//bot->Notice(theClient, "I generated this password for you: \002%s\002",
//      plainpass.c_str());
bot->Notice(theClient, "Successfully created user %s with password %s",newUser->getUserName().c_str(),plainpass.c_str());
bot->Notice(theClient, "Login using \002/msg %s@%s LOGIN %s %s\002",
        bot->getNickName().c_str(),
        bot->getUplinkName().c_str(),
        st[1].c_str(),
        plainpass.c_str());

delete (newUser);

//bot->Notice(theClient, "Then change your password using \002/msg "
//      "%s@%s NEWPASS <new_password>\002",
//      bot->getNickName().c_str(),
//      bot->getUplinkName().c_str());

//Record the IP obly if below the required level
if (admLevel < level::hello)
{
bot->helloIPList.erase(theClient->getIP());
bot->helloIPList.insert(
        std::make_pair(theClient->getIP(),
                bot->currentTime() + bot->helloBlockPeriod) );
}

//delete (newUser);
#endif //ALLOW_HELLO
return true ;
}

} // namespace gnuworld.

