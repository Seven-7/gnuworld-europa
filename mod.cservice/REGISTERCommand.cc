/**
 * REGISTERCommand.cc
 *
 * 26/12/2000 - Greg Sikorski <gte@atomicrevs.demon.co.uk>
 * Initial Version.
 *
 * Registers a channel.
 *
 * Caveats: None
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 * $Id: REGISTERCommand.cc,v 1.24 2009/07/31 07:29:13 mrbean_ Exp $
 */

#include	<map>
#include	<string>
#include	<sstream>
#include	<iostream>

#include	"StringTokenizer.h"
#include	"ELog.h"
#include	"cservice.h"
#include	"levels.h"
#include	"dbHandle.h"
#include	"Network.h"
#include	"responses.h"


const char REGISTERCommand_cc_rcsId[] = "$Id: REGISTERCommand.cc,v 1.24 2009/07/31 07:29:13 mrbean_ Exp $" ;

namespace gnuworld
{
using std::pair ;
using std::string ;
using std::endl ;
using std::ends ;
using std::stringstream ;

using namespace gnuworld;

bool REGISTERCommand::Exec( iClient* theClient, const string& Message )
{
	bot->incStat("COMMANDS.REGISTER");

	StringTokenizer st( Message ) ;

	/*
	 *  Fetch the sqlUser record attached to this client. If there isn't one,
	 *  they aren't logged in - tell them they should be.
	 */

	sqlUser* theUser = bot->isAuthed(theClient, true);
	if (!theUser) return false;

	if( st.size() < 2 )
	{
		Usage(theClient);
		return true;
	}

	string::size_type pos = st[1].find_first_of( ',' ); /* Don't allow comma's in channel names. :) */

	if ( (st[1][0] != '#') || (string::npos != pos))
	{
		bot->Notice(theClient,
			bot->getResponse(theUser,
				language::inval_chan_name,
				string("Invalid channel name.")));
		return false;
	}

	/*
	 *  First, check the channel isn't already registered.
	 */

	sqlChannel* theChan;
	theChan = bot->getChannelRecord(st[1]);
	if (theChan)
	{
		bot->Notice(theClient,
			bot->getResponse(theUser,
				language::chan_already_reg,
				string("%s is already registered with me.")).c_str(),
			st[1].c_str());
		return false;
	}

	/*
	 *  Check the user has sufficient access for this command..
	 */

	int level = bot->getAdminAccessLevel(theUser);
	if (((level < level::registercmd) && (level > 0)) && (st.size() > 2))
	{
		bot->Notice(theClient,
			bot->getResponse(theUser,
				language::insuf_access,
				string("You have insufficient access to perform that command.")));
		return false;
	}

	// Admin registration case !
	if ((level >= level::registercmd) && (st.size() > 2))
	{
		sqlUser* tmpUser = bot->getUserRecord(st[2]);
		if (!tmpUser)
		{
			bot->Notice(theClient,
					bot->getResponse(theUser,
							language::not_registered,
							string("The user %s doesn't appear to be registered.")).c_str(),
							st[2].c_str());
			return true;
		}

		if (level < level::immune::registercmd)
		{
			if (!bot->isValidChannel(st[1]))
			{
				if (!bot->validResponseString.empty())
				{
					bot->Notice(theClient, "Cannot register, channel is %s",bot->getCurrentValidResponse().c_str());
					bot->validResponseString.clear();
				}
	           	return false;
			}

			if (!bot->isValidUser(tmpUser->getUserName()))
			{
				if (!bot->validResponseString.empty())
				{
					bot->Notice(theClient, "Cannot register, invalid target user (%s)",bot->getCurrentValidResponse().c_str());
					bot->validResponseString.clear();
				}
	           	return false;
			}

			if (!bot->isValidApplicant(tmpUser))
			{
				if (!bot->validResponseString.empty())
				{
					if (bot->getCurrentValidResponse() == "ALREADY_HAVE_CHAN")
					{
						bot->Notice(theClient, "Target user already have a channel registered.");
						bot->validResponseString.clear();
						return false;
					}
					if (bot->getCurrentValidResponse() == "ALREADY_HAVE_PENDINGCHAN")
					{
						bot->Notice(theClient, "Target user already have a channel pending registration.");
						bot->validResponseString.clear();
						return false;
					}
					bot->Notice(theClient, bot->getCurrentValidResponse().c_str());
					bot->validResponseString.clear();
				}
	           	return false;
			}
		} //level < immune::registercomd

		if (bot->sqlRegisterChannel(theClient, tmpUser,st[1].c_str()))
		{
			sqlChannel* newChan = bot->getChannelRecord(st[1]);
			bot->writeChannelLog(newChan, theClient, sqlChannel::EV_REGISTER, "to " + tmpUser->getUserName());
			bot->logAdminMessage("%s (%s) has registered %s to %s", theClient->getNickName().c_str(),
					theUser->getUserName().c_str(), st[1].c_str(), tmpUser->getUserName().c_str());

			bot->Notice(theClient,
                bot->getResponse(theUser,
                	language::regged_chan,
                    	string("Registered channel %s to %s")).c_str(), st[1].c_str(),tmpUser->getUserName().c_str());
			//return true;
		} else {
			if (!bot->validResponseString.empty())
			{
				bot->Notice(theClient, bot->validResponseString.c_str());
				bot->validResponseString.clear();
			}
                	return false;
			}
		return true;
	} // end of Admin registration case

	//Normal user application case
	if ((level == 0) || (st.size() < 3))
	{
		bot->logDebugMessage("Performing normal registration");

		if (!bot->isValidChannel(st[1]))
		{
			if (!bot->validResponseString.empty())
			{
				bot->Notice(theClient, "Cannot register, channel is %s",bot->getCurrentValidResponse().c_str());
				bot->validResponseString.clear();
			}
           	return false;
		}

		if (!bot->isValidUser(theUser->getUserName()))
		{
			if (!bot->validResponseString.empty())
			{
				bot->Notice(theClient, "Cannot register, Your username is invalid (%s)",bot->getCurrentValidResponse().c_str());
				bot->validResponseString.clear();
			}
           	return false;
		}

		if (!bot->isValidApplicant(theUser))
		{
			if (!bot->validResponseString.empty())
			{
				if (bot->getCurrentValidResponse() == "TOO_NEW")
				{
					bot->Notice(theClient, "Sorry, You can't register new channels at the moment.");
					bot->Notice(theClient, "Your username must have been created since at least %i days in order to apply for a new channel.",bot->MinDaysBeforeReg);
					bot->validResponseString.clear();
					return false;
				}
				if (bot->getCurrentValidResponse() == "ALREADY_HAVE_CHAN")
				{
					bot->Notice(theClient, "Sorry, you already have a channel registered to you");
					bot->Notice(theClient, "You can only register \002ONE\002 channel.");
					bot->validResponseString.clear();
					return false;
				}
				if (bot->getCurrentValidResponse() == "ALREADY_HAVE_PENDINGCHAN")
				{
					bot->Notice(theClient, "Sorry, you already have a channel pending registeration to you");
					bot->Notice(theClient, "You can only register \002ONE\002 channel.");
					bot->validResponseString.clear();
					return false;
				}
				bot->Notice(theClient, bot->getCurrentValidResponse().c_str());
				bot->validResponseString.clear();
			}
           	return false;
		}

		//The instant registration case
		if (bot->RequiredSupporters == 0)
		{
			if (bot->sqlRegisterChannel(bot->getInstance(), theUser, st[1].c_str()))
			{
				sqlChannel* newChan = bot->getChannelRecord(st[1]);
				bot->writeChannelLog(newChan, bot->getInstance(), sqlChannel::EV_REGISTER, "to " + theUser->getUserName());
				bot->logAdminMessage("%s (%s) has registered %s to %s", bot->getNickName().c_str(),
						bot->getInstance()->getNickUserHost().c_str(), st[1].c_str(), theUser->getUserName().c_str());

				bot->Notice(theClient,
			                bot->getResponse(theUser,
	        		        	language::regged_chan,
	                    		string("Registered channel %s to %s")).c_str(), st[1].c_str(), theUser->getUserName().c_str());
				//return true;
			} else {
				if (!bot->validResponseString.empty())
				{
					bot->Notice(theClient, bot->validResponseString.c_str());
					bot->logDebugMessage("The Judge Failed to (instant) register channel %s with reason:", st[1].c_str());
					bot->logDebugMessage(bot->validResponseString.c_str());
					bot->validResponseString.clear();
				}
	                	return false;
				}
			return true;
		}

		bot->logDebugMessage("Entering REGISTER.CheckReclaimQuery");
		//Add the registration query into the db (fresh insert, or reclaim)
		//Check if the channel exists in the 'channels' table with the registered_ts=0 than it was wiped,
		//and we need to do a reclaim
		bool reclaim = false;
		int chanId = 0;
		stringstream theQuery;
		theQuery	<< "SELECT id FROM channels WHERE lower(name) = '"
					<< escapeSQLChars(string_lower(st[1]))
					<< "' AND registered_ts = 0"
					<< ends;
		if (!bot->SQLDb->Exec(theQuery, true))
		{
			bot->logDebugMessage("Error on REGISTER.CheckReclaimQuery");
		#ifdef LOG_SQL
			//elog << "sqlQuery> " << theQuery.str().c_str() << endl;
			elog 	<< "REGISTER.CheckReclaimQuery> SQL Error: "
		     		<< bot->SQLDb->ErrorMessage()
		     		<< endl ;
		#endif
			return false;
		} else if (bot->SQLDb->Tuples() == 1)
		{
			chanId = atoi(bot->SQLDb->GetValue(0,0));
			reclaim = true;
			bot->logDebugMessage("RECLAIMING is Necessary");
		}
			//TODO: !! logChannel("New Incoming Application");

		bot->logDebugMessage("Entering RECLAIM");

		if (reclaim)
		{
			bot->wipeChannel(chanId);
			theQuery.str("");
			theQuery	<< "UPDATE channels SET name = '"
						<< escapeSQLChars(st[1])
						<< "', mass_deop_pro=0, flood_pro=0, flags=0, limit_offset=3, "
						<< "limit_period=20, limit_grace=1, limit_max=0, userflags=0, "
						<< "url='', description='', keywords='', registered_ts=0, "
						<< "channel_ts=0, channel_mode='', comment='', last_updated=now()::abstime::int4 "
						<< "WHERE id = " << chanId
						<< ends;
			if (!bot->SQLDb->Exec(theQuery, true))
			{
				bot->logDebugMessage("Error on REGISTER.DoReclaimQuery");
				#ifdef LOG_SQL
				//elog << "sqlQuery> " << theQuery.str().c_str() << endl;
				elog 	<< "REGISTER.DoReclaimQuery> SQL Error: "
				   		<< bot->SQLDb->ErrorMessage()
				   		<< endl ;
				#endif
				return false;
			} else //if (bot->SQLDb->Tuples() > 0)
				bot->logDebugMessage("Successfully reclaimed channel %s", st[1].c_str());
		}
		else //reclaim
		{
			theQuery.str("");
			theQuery	<< "INSERT INTO channels (name,url,description,keywords,registered_ts,"
						<< "channel_ts,channel_mode,comment,last_updated,mass_deop_pro,flood_pro,"
						<< "flags,limit_offset,limit_period,limit_grace,limit_max,userflags"
						<< ") VALUES ('"
						<< escapeSQLChars(st[1])
						<< "','','','',0,0,'','',now()::abstime::int4,0,0,0,3,20,1,0,0)"
						<< ends;
			if (!bot->SQLDb->Exec(theQuery, true))
			{
				bot->logDebugMessage("Error on REGISTER.InsertIntoQuery");
				#ifdef LOG_SQL
				//elog << "sqlQuery> " << theQuery.str().c_str() << endl;
				elog 	<< "REGISTER.InsertIntoQuery> SQL Error: "
				   		<< bot->SQLDb->ErrorMessage()
				   		<< endl ;
				#endif
				return false;
			} 
			else  //if (bot->SQLDb->Tuples() > 0)
			{	//Also here we need to find out the new channels's id
                		theQuery.str("");
                		theQuery        << "SELECT id FROM channels WHERE name = '"
                        		        << escapeSQLChars(st[1])
                                		<< "'"
                                		<< ends;
                        	if (!bot->SQLDb->Exec(theQuery, true))
                        	{
                                	bot->logDebugMessage("Error on REGISTER.GetNewChannelIDQuery");
                                	#ifdef LOG_SQL
                                	//elog << "sqlQuery> " << theQuery.str().c_str() << endl;
                                	elog    << "REGISTER.GetNewChannelIDQuery> SQL Error: "
                                                << bot->SQLDb->ErrorMessage()
                                                << endl ;
                                	#endif
                                	return false;
                        	} //else bot->logDebugMessage("Successfully get id for channel %s", st[1].c_str());
                        	else if (bot->SQLDb->Tuples() > 0) chanId = atoi(bot->SQLDb->GetValue(0,0));
			
				bot->logDebugMessage("Successfully inserted new channel %s with id = %i", st[1].c_str(), chanId);
			}
		}
		//bot->logDebugMessage("EXITING");
		
		sqlIncompleteChannel* newApp = new (std::nothrow) sqlIncompleteChannel(bot->SQLDb);
		assert(newApp != 0);

		bot->incompleteChanRegs.insert(cservice::incompleteChanRegsType::value_type(theUser->getID(),newApp));
		
		newApp->chanName = st[1];
		newApp->chanId = chanId;
		if (!newApp->commitNewPending(theUser->getID(), chanId))
		{
			bot->logDebugMessage("Error on REGISTER.commitNewPending");
			return false;
		}

		sqlIncompleteChannel *tmpApp = bot->incompleteChanRegs.find(theUser->getID())->second;

		//Ask after the Real Name

		bot->Notice(theClient,"Now specify your Real Name by typing /msg %s REGISTER %s REALNAME <Your RealName>", bot->getNickName().c_str(), tmpApp->chanName.c_str());//st[1].c_str());
		bot->Notice(theClient,"To cancel/abort your application any time, type /msg %s CANCEL %s", bot->getNickName().c_str(), tmpApp->chanName.c_str());//st[1].c_str());

		//Ask channel's description
		bot->Notice(theClient,"Now specify your channel's description by typing /msg %s REGISTER %s DESC <description>", bot->getNickName().c_str(),st[1].c_str());
		bot->Notice(theClient,"To cancel/abort your application any time, type /msg %s CANCEL %s", bot->getNickName().c_str(),st[1].c_str());

		//Ask the list of supporters
		bot->Notice(theClient,"Now enumerate your supporters by typing /msg %s REGISTER %s SUPPORTERS <supporter1 supporter2 supporter3 ...>", bot->getNickName().c_str(),st[1].c_str());
		bot->Notice(theClient,"To cancel/abort your application any time, type /msg %s CANCEL %s", bot->getNickName().c_str(),st[1].c_str());

		//Check the validity of the list of the supporters (Huge work)
	}
	return true;
}

} // namespace gnuworld.
